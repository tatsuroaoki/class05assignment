// If an li element is clicked, toggle the class "done" on the <li>
const checkComplete = function(e) {
	if (this.parentElement.classList.contains('done')) {
		this.parentElement.classList.remove('done');
	} else {
		this.parentElement.classList.add('done')
	}
}

// If a delete link is clicked, delete the li element / remove from the DOM
const deleteItem = function(e) {
	//	const parentItem = this.parentElement;
	e.target.parentElement.remove('li');
}

// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
//const itemMove = document.querySelectorAll('a.move');
//console.log(itemMove);
//console.log(itemMove[0])
//console.log(itemMove[1])

const moveLists = function(e) {
	let parentList;
	if (this.parentElement.parentElement.className === 'today-list') {
		parentList = document.getElementsByClassName('later-list')[0];
		this.parentElement.remove;
		parentList.appendChild(this.parentElement);
		this.innerHTML = 'Move to Today';
		this.classList.remove('toLater');
		this.classList.add('toToday');
	} else {
		parentList = document.getElementsByClassName('today-list')[0];
		this.parentElement.remove;
		parentList.appendChild(this.parentElement);
		this.innerHTML = 'Move to Later';
		this.classList.remove('toToday');
		this.classList.add('toLater');
	}
}

// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
	e.preventDefault();
	const input = this.parentNode.getElementsByTagName('input')[0];
	const text = input.value; // use this text to create a new <li>

	// Finish function here

	// Create elements for new item
	const newListItem = document.createElement('li');
	const newListItemDesc = document.createElement('span');
	const newListItemName = document.createTextNode(text);
	const newItem = newListItem.appendChild(newListItemDesc);
	newListItemDesc.appendChild(newListItemName);

	// Add elements to enable moving and deleting of new item
	const newMoveButton = document.createElement('a');
	const newDeleteButton = document.createElement('a');

	// If adding to Today List
	if (this.parentElement.previousElementSibling.className === 'today-list') {
		newMoveButton.setAttribute('class', 'move toLater');
		newMoveButton.innerHTML = 'Move to Later';
		// If adding to Later List
	} else {
		newMoveButton.setAttribute('class', 'move toToday');
		newMoveButton.innerHTML = 'Move to Today';
	}
	newListItem.appendChild(newMoveButton);
	newDeleteButton.setAttribute('class', 'delete');
	newDeleteButton.innerHTML = 'Delete';
	newListItem.appendChild(newDeleteButton);
	input.value = '';

	// Add to complete item to list
	const addToList = this.parentNode.parentNode.getElementsByTagName('ul')[0];
	const newToDo = addToList.appendChild(newListItem);

	// Check new item
	newToDo.firstChild.addEventListener('click', checkComplete);

	// Delete new item
	newToDo.querySelectorAll('a.delete')[0].addEventListener('click', deleteItem);

	// Move new item
	newToDo.getElementsByClassName('move')[0].addEventListener('click', moveLists);
}

// Add this as a listener to the two Add links
const addItemArray = document.getElementsByClassName('add-item');
for (let i = 0; i < addItemArray.length; i++) {
	addItemArray[i].addEventListener('click', addListItem);
}

// Listener for done
const itemArray = document.querySelectorAll('li');
for (let i = 0; i < itemArray.length; i++) {
	console.log(itemArray[i].firstElementChild);
	itemArray[i].firstElementChild.addEventListener('click', checkComplete);
}

// Listener for delete
const deleteItemArray = document.querySelectorAll('a.delete');
for (let i = 0; i < deleteItemArray.length; i++) {
	deleteItemArray[i].addEventListener('click', deleteItem);
}

// Listener for moving
const moveList = document.getElementsByClassName('move');
for (i = 0; i < moveList.length; i++) {
	moveList[i].addEventListener('click', moveLists)
}