// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
const cta = document.createElement('a');
const ctaText = document.createTextNode('Buy now!');
cta.appendChild(ctaText);
cta.setAttribute('id','cta');
const main = document.getElementsByTagName('main')[0];
main.appendChild(cta);

// Access (read) the data-color attribute of the <img>,
// log to the console
const carImg = document.getElementsByTagName('img')[0];
const dataColor = carImg.dataset.color;
console.log(dataColor);

// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
const liEls = document.getElementsByTagName('li');
liEls[2].className += 'highlight';
	
// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
const lastP = main.lastChild.previousElementSibling;
console.log(main.lastChild.previousElementSibling);
const removeLastP = main.removeChild(lastP);