// IN-CLASS EXERCISE #1: TRAVERSING THE DOM WITH NODES
let body = document.getElementsByTagName('body')[0];
console.log(body.firstChild);

let ul = document.getElementsByTagName('ul')[0];
console.log(ul.parentNode.parentNode);

let p = document.getElementsByTagName('p')[0];
console.log(p.previousSibling.lastChild);

// IN-CLASS EXERCISE #2: DOM CRUD - see dom-crud.js

// IN-CLASS EXERCISE #3: PLUSES AND MINUSES
let counter = 0;
const plusEl = document.getElementById('plus');
const minusEl = document.getElementById('minus');
const counterEl = document.getElementById('counter');

plusEl.addEventListener('click',function(e) { 
	counter++;
	counterEl.innerHTML = counter;
	console.log(counter);
})

minusEl.addEventListener('click',function(e) {
	counter--;
	counterEl.innerHTML = counter;
	console.log(counter);
})